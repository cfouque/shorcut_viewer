#include "ShortcutWidget.hpp"

ShortcutWidget::ShortcutWidget() : 
    QTableWidget() 
{
    setColumnCount(2) ;
    setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void ShortcutWidget::populate()
{
    const QMetaObject & mo = QKeySequence::staticMetaObject ;
    int index = mo.indexOfEnumerator("StandardKey") ;
    QMetaEnum standardKeyEnum = mo.enumerator(index) ;
    
    setRowCount( standardKeyEnum.keyCount() );
    
    QString scope = standardKeyEnum.scope() ;
    for ( int i = 0 ; i < standardKeyEnum.keyCount() ; ++i ) 
    {
        if ( standardKeyEnum.value(i) == -1 )
            continue ;
        
        QString key = scope + "::" + standardKeyEnum.key(i) ;
        
        QKeySequence::StandardKey sk = static_cast<QKeySequence::StandardKey>( standardKeyEnum.value(i) ) ;
        QKeySequence ks = QKeySequence( sk ) ;
        
        setItem( i , 0 , new QTableWidgetItem(key) ) ;
        setItem( i , 1 , new QTableWidgetItem(ks.toString()) );
        
    }
    resizeColumnsToContents();
}
