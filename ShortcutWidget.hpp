/*!
 * 
 */

#ifndef __SHORCUT_WIDGET_HPP__
#define __SHORCUT_WIDGET_HPP__

#include <QtWidgets>

class ShortcutWidget final : public QTableWidget 
{
    
    Q_OBJECT
    
public :
    explicit ShortcutWidget() ;
    void populate() ;
    
} ;

#endif 