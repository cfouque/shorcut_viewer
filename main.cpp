#include <QApplication> 
#include "ShortcutWidget.hpp"

int main(int argc, char *argv[]) 
{
    QApplication app(argc, argv);
    ShortcutWidget widget ;
    widget.populate() ;
    widget.show() ;
    
    return app.exec() ;
    
}